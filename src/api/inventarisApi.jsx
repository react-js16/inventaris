import axios from "axios"

const token = localStorage.getItem('token');

const inventarisApi = {
    getCurrentUser: async () => {
        const res = await axios.get('http://inventaris-api.test/api/user', {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    getUsers: async (url) => {
        const res = await axios.get(url, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        });

        return res;
    },
    updateUser: async (id, formData) => {
        const res = await axios.post('http://inventaris-api.test/api/update/user/' + id, formData, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    deleteUser: async (id) => {
        const res = await axios.delete('http://inventaris-api.test/api/delete/user/' + id, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    getProducts: async (url) => {
        const res = await axios.get(url, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    addProduct: async (formData) => {
        const res = await axios.post('http://inventaris-api.test/api/store/produk', formData, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    updateProduct: async (id, formData) => {
        const res = await axios.post('http://inventaris-api.test/api/update/produk/' + id, formData, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    deleteProduct: async (id) => {
        const res = await axios.delete('http://inventaris-api.test/api/delete/produk/' + id, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    getMaterial: async (id) => {
        const res = await axios.get('http://inventaris-api.test/api/detail/produk/' + id, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    addMaterial: async (formData) => {
        const res = await axios.post('http://inventaris-api.test/api/store/material', formData, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    updateMaterial: async (id, formData) => {
        const res = await axios.post('http://inventaris-api.test/api/update/material/' + id, formData, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    },
    deleteMaterial: async (id) => {
        const res = await axios.delete('http://inventaris-api.test/api/delete/material/' + id, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        return res;
    }
}

export default inventarisApi;