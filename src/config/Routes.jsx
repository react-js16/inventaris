import React from 'react';

import { Route, Switch } from 'react-router-dom';

import Home from '../pages/Home';
import Settings from '../pages/Settings';
import User from '../pages/Users';
import Archive from '../pages/Archive';
import Products from '../pages/Products';
import Material from '../pages/Material';

const Routes = ({user}) => {
    return (
        <Switch>
            <Route
                path='/'
                exact
                component={Home}
            />
            <Route
                path='/settings'
                render={(props) => <Settings user={user} {...props} />}
            />
            <Route
                path='/users'
                component={User}
            />
            <Route
                path='/archive'
                component={Archive}
            />
            <Route
                path='/products'
                component={Products}
            />
            <Route
                path='/material/:id'
                component={Material}
            />
        </Switch>
    );
};

export default Routes;