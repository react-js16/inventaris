import {
    Badge,
    Box,
    useColorModeValue,
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableContainer,
    IconButton,
    ButtonGroup,
    useDisclosure,
    Stack,
    Button,
    Center,
    Text,
    Link,
    Divider
} from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import inventarisApi from '../api/inventarisApi';
import {
    DeleteIcon,
    EditIcon,
    AddIcon
} from '@chakra-ui/icons';
import {
    FiFile
} from 'react-icons/fi';
import ModalAdd from '../components/material/ModalAdd';
import ModalEdit from '../components/material/ModalEdit';
import Alert from '../components/material/Alert';

const Material = () => {

    const {id} = useParams()
    const [produk, setProduk] = useState([])
    const [materials, setMaterials] = useState([])
    const [material, setMaterial] = useState()
    const [price, setPrice] = useState()
    const [qty, setQty] = useState()
    const [link, setLink] = useState()
    const [Id, setId] = useState()

    const {
        isOpen: isAddOpen,
        onClose: onAddClose,
        onOpen: onAddOpen
    } = useDisclosure()

    const {
        isOpen: isEditOpen,
        onClose: onEditClose,
        onOpen: onEditOpen
    } = useDisclosure()

    const {
        isOpen: isAlertOpen,
        onClose: onAlertClose,
        onOpen: onAlertOpen
    } = useDisclosure()

    const getDetail = () => {
        const getMaterial = inventarisApi.getMaterial(id)
        getMaterial.then((res) => {
            // console.log(res.data)
            setProduk(res.data.produk)
            setMaterials(res.data.material)
        })
    }

    const handleExport = e => {
        e.preventDefault(e)

        console.log(e)
    }
    
    useEffect(() => {
        getDetail()
    }, [])

    return (
        <>
            <Box
                bg={useColorModeValue('white', 'black')}
                rounded={'lg'}
                p={[3, 4]}
                mb={5}
            >
                <Box display='flex' alignItems='baseline' mb={3}>
                    <Badge borderRadius='full' px='2' colorScheme='teal'>
                        Nama Produk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                    </Badge>
                    <Box
                        fontWeight='semibold'
                        letterSpacing='wide'
                        fontSize='xs'
                        textTransform='uppercase'
                        ml='2'
                    >
                        {produk && produk.nama_produk}
                    </Box>
                </Box>
                <Box display='flex' alignItems='baseline' mb={3}>
                    <Badge borderRadius='full' px='2' colorScheme='teal'>
                        Deskripsi Produk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                    </Badge>
                    <Box
                        fontWeight='semibold'
                        letterSpacing='wide'
                        fontSize='xs'
                        textTransform='uppercase'
                        ml='2'
                    >
                        {produk && produk.deskripsi_produk}
                    </Box>
                </Box>
                <Box display='flex' alignItems='baseline' mb={3}>
                    <Badge borderRadius='full' px='2' colorScheme='teal'>
                        Harga&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                    </Badge>
                    <Box
                        fontWeight='semibold'
                        letterSpacing='wide'
                        fontSize='xs'
                        textTransform='uppercase'
                        ml='2'
                    >
                        {produk && produk.harga}
                    </Box>
                </Box>
            </Box>
            <Box
                bg={useColorModeValue('white', 'black')}
                rounded={'lg'}
                p={[3, 4]}
            >
                <Button mt={5} ml={5} mb={2} leftIcon={<AddIcon />} colorScheme='teal' size='sm' onClick={() => onAddOpen()}>
                    Add Material
                </Button>
                <Button mt={5} ml={2} mb={2} leftIcon={<FiFile />} colorScheme='teal' size='sm' onClick={e => handleExport(e)}>
                    Export
                </Button>
                <TableContainer>
                    <Table variant='simple'>
                        <Thead>
                            <Tr>
                                <Th>Material</Th>
                                <Th>Price</Th>
                                <Th>Qty</Th>
                                <Th>Link</Th>
                                <Th>

                                </Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {
                                materials?.map((material, i) => {
                                    return (
                                        <Tr key={i}>
                                            <Td>{material.nama_material}</Td>
                                            <Td>{material.harga}</Td>
                                            <Td>{material.jumlah}</Td>
                                            <Td>
                                                <Link href={material.link}>
                                                    {material.link}
                                                </Link>
                                            </Td>
                                            <Td textAlign={'right'}>
                                                <ButtonGroup>
                                                    <IconButton
                                                        variant='ghost'
                                                        colorScheme={'blue'}
                                                        aria-label='edit'
                                                        icon={<EditIcon/>}
                                                        onClick={() => {
                                                            onEditOpen()
                                                            setMaterial(material.nama_material)
                                                            setPrice(material.harga)
                                                            setQty(material.jumlah)
                                                            setLink(material.link)
                                                            setId(material.id)
                                                        }}
                                                    />
                                                    <IconButton
                                                        variant='ghost'
                                                        colorScheme={'red'}
                                                        aria-label='delete'
                                                        icon={<DeleteIcon/>}
                                                        onClick={() => {
                                                            setId(material.id)
                                                            onAlertOpen()
                                                        }}
                                                    />
                                                </ButtonGroup>
                                            </Td>
                                        </Tr>
                                    )
                                })
                            }
                        </Tbody>
                        <Tfoot>
                            <Tr>
                                <Th>Material</Th>
                                <Th>Price</Th>
                                <Th>Qty</Th>
                                <Th>Link</Th>
                                <Th>
                                    
                                </Th>
                            </Tr>
                        </Tfoot>
                    </Table>
                </TableContainer>
                <ModalAdd isOpen={isAddOpen} onClose={onAddClose} getdetail={getDetail} id={id}/>
                <ModalEdit isOpen={isEditOpen} onClose={onEditClose} getdetail={getDetail} id={id} Id={Id} material={material} price={price} qty={qty} link={link}/>
                <Alert isOpen={isAlertOpen} onClose={onAlertClose} getdetail={getDetail} id={id} Id={Id}/>
            </Box>
        </>
    );
};

export default Material;