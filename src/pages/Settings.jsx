import { Box } from '@chakra-ui/react';
import React from 'react';
import NavItem from '../components/NavItem';
import {
    FiPlusCircle,
    FiUsers
} from 'react-icons/fi';

const Settings = ({user}) => {
    return (
        <Box>
            {
                user && user.role == 1 ? 
                <NavItem icon={FiUsers} href={'/users'}>
                    Users
                </NavItem>
                :
                null
            }
            <NavItem icon={FiPlusCircle} href={'/register'}>
                New User
            </NavItem>
        </Box>
    );
};

export default Settings;