import React, { useState, useEffect } from 'react';

import {
    Box,
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableContainer,
    useColorModeValue,
    IconButton,
    ButtonGroup,
    useDisclosure,
    Stack,
    Button,
    Center,
    Text,
} from '@chakra-ui/react';

import inventarisApi from '../api/inventarisApi';

import {
    DeleteIcon,
    EditIcon,
    AddIcon
} from '@chakra-ui/icons';
import ModalAdd from '../components/product/ModalAdd';
import ModalEdit from '../components/product/ModalEdit';
import Alert from '../components/product/Alert';
import { Link } from 'react-router-dom';

const Products = () => {

    const [products, setProducts] = useState([]);
    const [id, setId] = useState('');
    const [product, setProduct] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [paginate, setPaginate] = useState([]);
    const [active, setActive] = useState('http://inventaris-api.test/api/get/produk?page=1');

    const {
        isOpen: isAddOpen,
        onOpen: onAddOpen,
        onClose: onAddClose
    } = useDisclosure()

    const {
        isOpen: isEditOpen,
        onOpen: onEditOpen,
        onClose: onEditClose
    } = useDisclosure();

    const {
        isOpen: isAlertOpen,
        onOpen: onAlertOpen,
        onClose: onAlertClose
    } = useDisclosure();

    const Products = (url) => {
        const getProducts = inventarisApi.getProducts(url);
        getProducts.then((res) => {
            // console.log(res)
            setProducts(res.data.produk.data)
            setPaginate(res.data.produk)
        }).catch((error) => {
            // console.log(error.response.status);
            if(error.response.status === 401) {
                localStorage.removeItem('token');
                window.location.replace('/login');
            }
        })
    }

    useEffect(() => {
        Products('http://inventaris-api.test/api/get/produk?page=1')
    }, [])

    return (
        <Box
            bg={useColorModeValue('white', 'black')}
            rounded={'lg'}
            p={[3, 4]}
        >
            <Button m={5} leftIcon={<AddIcon />} colorScheme='teal' size='sm' onClick={() => onAddOpen()}>
                Add Product
            </Button>
            <TableContainer>
                <Table variant='simple'>
                    <Thead>
                        <Tr>
                            <Th>Product</Th>
                            <Th>Description</Th>
                            <Th>Price</Th>
                            <Th>

                            </Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {
                            products?.map((product, i) => {
                                return (
                                    <Tr key={i}>
                                        <Td>{product.nama_produk}</Td>
                                        <Td>{product.deskripsi_produk}</Td>
                                        <Td>{product.harga}</Td>
                                        <Td textAlign={'right'}>
                                            <ButtonGroup>
                                                <Link
                                                    to={'/material/' + product.id}
                                                >
                                                    <IconButton
                                                        variant='ghost'
                                                        colorScheme={'teal'}
                                                        aria-label='edit'
                                                        icon={<AddIcon/>}
                                                    />
                                                </Link>
                                                <IconButton
                                                    variant='ghost'
                                                    colorScheme={'blue'}
                                                    aria-label='edit'
                                                    icon={<EditIcon/>}
                                                    onClick={() => {
                                                        setId(product.id)
                                                        setProduct(product.nama_produk)
                                                        setDescription(product.deskripsi_produk)
                                                        setPrice(product.harga)
                                                        onEditOpen()
                                                    }}
                                                />
                                                <IconButton
                                                    variant='ghost'
                                                    colorScheme={'red'}
                                                    aria-label='delete'
                                                    icon={<DeleteIcon/>}
                                                    onClick={() => {
                                                        setId(product.id)
                                                        onAlertOpen()
                                                    }}
                                                />
                                            </ButtonGroup>
                                        </Td>
                                    </Tr>
                                )
                            })
                        }
                    </Tbody>
                    <Tfoot>
                        <Tr>
                            <Th>Product</Th>
                            <Th>Description</Th>
                            <Th>Price</Th>
                            <Th>
                                
                            </Th>
                        </Tr>
                    </Tfoot>
                </Table>
            </TableContainer>
            <Center>
                <Stack direction={['column', 'row']} spacing='1px' mt={3}>
                    {paginate.links && paginate.links.map(item => (
                            <Button colorScheme='teal' key={item.label} onClick={() => {Products(item.url); setActive(item.url)}} variant={item.active ? 'solid' : 'outline'}>
                                {item.label}
                            </Button>
                    ))}
                </Stack>
            </Center>
            <Center>
                <Text mt={3}>Showing {paginate.from} to {paginate.to} of {paginate.total} entries</Text>
            </Center>
            <ModalAdd isOpen={isAddOpen} onClose={onAddClose} products={Products} active={active}/>
            <ModalEdit isOpen={isEditOpen} onClose={onEditClose} products={Products} active={active} product={product} description={description} price={price} id={id}/>
            <Alert isOpen={isAlertOpen} onClose={onAlertClose} products={Products} active={active} id={id}/>
        </Box>
    );
};

export default Products;