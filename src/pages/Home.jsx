import React, { useEffect } from 'react';

import {
    Box,
} from '@chakra-ui/react'

const Home = () => {

    useEffect(() => {
        const token = localStorage.getItem('token');

        if(!token) {
            window.location.replace('/login');
        }
    }, [])

    return (
        <Box
            p='5'
        >
            
        </Box>
    );
};

export default Home;