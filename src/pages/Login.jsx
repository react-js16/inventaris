import React, { useEffect, useState } from 'react';

import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    Stack,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue,
    useToast,
} from '@chakra-ui/react';
import axios from 'axios';

const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const toast = useToast();

    const login = async () => {
        const formData = new FormData();
        formData.append('email', email);
        formData.append('password', password);

        await axios.post('http://inventaris-api.test/api/login', formData, {
        }).then((res) => {
            if(res.data.message) {
                toast({
                    title: 'Failed',
                    status: 'error',
                    description: res.data.message,
                    duration: 9000,
                    isClosable: true,
                })
            }
            localStorage.setItem('token', res.data.token);
            // console.log(res.data)
            if(res.data.success == true) {
                window.location.replace('/');
            }
        }).catch((error) => {
            // console.log(error.response);
            if(error.response.status == 422) {
                toast({
                    title: error.response.statusText,
                    status: 'error',
                    description: 'Email and Password is Required',
                    duration: 9000,
                    isClosable: true,
                })
            }
        })
    }

    const handleLogin = (e) => {
        e.preventDefault();
        login();
    }

    useEffect(() => {
        const token = localStorage.getItem('token');

        if(token) {
            window.location.replace('/');
        }
    }, []);

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.50', 'gray.800')}
        >
            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
                <Stack align={'center'}>
                    <Heading fontSize={'4xl'}>Sign in to your account</Heading>
                    <Text fontSize={'lg'} color={'gray.600'}>
                        to enjoy all of our cool <Link color={'teal.400'}>features</Link> ✌️
                    </Text>
                </Stack>
                <Box
                    rounded={'lg'}
                    bg={useColorModeValue('white', 'gray.700')}
                    boxShadow={'lg'}
                    p={8}
                >
                    <Stack spacing={4} as='form' onSubmit={(e) => handleLogin(e)}>
                        <FormControl id="email">
                            <FormLabel>Email address</FormLabel>
                            <Input
                                type="email"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </FormControl>
                        <FormControl id="password">
                            <FormLabel>Password</FormLabel>
                            <Input
                                type="password"
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </FormControl>
                        <Stack spacing={10}>
                            <Button
                                bg={'teal.400'}
                                color={'white'}
                                _hover={{
                                    bg: 'teal.500',
                                }}
                                type='submit'
                            >
                                Sign in
                            </Button  >
                        </Stack>
                    </Stack>
                </Box>
            </Stack>
        </Flex>
    );
};

export default Login;