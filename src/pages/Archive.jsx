import React from 'react';

import {
    Box
} from '@chakra-ui/react';

import NavItem from '../components/NavItem';

import {
    FiBox
} from 'react-icons/fi';

const Archieve = () => {
    return (
        <Box>
            <NavItem icon={FiBox} href={'/products'}>
                Products
            </NavItem>
        </Box>
    );
};

export default Archieve;