import React, { useEffect, useState } from 'react';
import {
    Box,
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableContainer,
    useColorModeValue,
    IconButton,
    ButtonGroup,
    useDisclosure,
    Stack,
    Button,
    Center,
    Text
} from '@chakra-ui/react';
import inventarisApi from '../api/inventarisApi';
import {
    DeleteIcon,
    EditIcon
} from '@chakra-ui/icons';
import ModalEdit from '../components/users/ModalEdit';
import Alert from '../components/users/Alert';

const Users = () => {

    const [users, setUsers] = useState([]);
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [paginate, setPaginate] = useState([]);
    const [active, setActive] = useState('http://inventaris-api.test/api/get/users?page=1');

    const {
        isOpen: isEditOpen,
        onOpen: onEditOpen,
        onClose: onEditClose
    } = useDisclosure();

    const {
        isOpen: isAlertOpen,
        onOpen: onAlertOpen,
        onClose: onAlertClose
    } = useDisclosure();

    const Users = (url) => {
        const getUsers = inventarisApi.getUsers(url);
        getUsers.then((res) => {
            // console.log(res.data.users.data);
            setUsers(res.data.users.data);
            setPaginate(res.data.users)
        }).catch((error) => {
            // console.log(error);
            if(error.response.status === 401) {
                localStorage.removeItem('token');
                window.location.replace('/login');
            }
        })
    }

    useEffect(() => {
        Users('http://inventaris-api.test/api/get/users?page=1')
    }, []);

    return (
        <>
            <Box
                bg={useColorModeValue('white', 'black')}
                rounded={'lg'}
                p={[3, 4]}
            >
                <TableContainer>
                    <Table variant='simple'>
                        <Thead>
                            <Tr>
                                <Th>Name</Th>
                                <Th>Email</Th>
                                <Th>
                                    
                                </Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {
                                users?.map((user, i) => {
                                    return (
                                        <Tr key={i}>
                                            <Td>{user.name}</Td>
                                            <Td>{user.email}</Td>
                                            <Td textAlign={'right'}>
                                                <ButtonGroup>
                                                    <IconButton
                                                        variant='ghost'
                                                        colorScheme={'blue'}
                                                        aria-label='edit'
                                                        icon={<EditIcon/>}
                                                        onClick={() => {
                                                            setId(user.id)
                                                            setName(user.name)
                                                            setEmail(user.email)
                                                            onEditOpen()
                                                        }} 
                                                    />
                                                    <IconButton
                                                        variant='ghost'
                                                        colorScheme={'red'}
                                                        aria-label='delete'
                                                        icon={<DeleteIcon/>}
                                                        onClick={() => {
                                                            setId(user.id)
                                                            onAlertOpen()
                                                        }}
                                                    />
                                                </ButtonGroup>
                                            </Td>
                                        </Tr>
                                    )
                                })
                            }
                        </Tbody>
                        <Tfoot>
                            <Tr>
                                <Th>Name</Th>
                                <Th>Email</Th>
                                <Th>
                                    
                                </Th>
                            </Tr>
                        </Tfoot>
                    </Table>
                </TableContainer>
                <Center>
                    <Stack direction={['column', 'row']} spacing='1px' mt={3}>
                        {paginate.links && paginate.links.map(item => (
                                <Button colorScheme='teal' key={item.label} onClick={() => {Users(item.url); setActive(item.url)}} variant={item.active ? 'solid' : 'outline'}>
                                    {item.label}
                                </Button>
                        ))}
                    </Stack>
                </Center>
                <Center>
                    <Text mt={3}>Showing {paginate.from} to {paginate.to} of {paginate.total} entries</Text>
                </Center>
                <ModalEdit isOpen={isEditOpen} onClose={onEditClose} id={id} name={name} email={email} users={Users} active={active}/>
                <Alert isOpen={isAlertOpen} onClose={onAlertClose} id={id} users={Users} active={active}/>
            </Box>
        </>
    );
};

export default Users;