import { BrowserRouter, Route } from "react-router-dom";
import Sidebar from "./components/Sidebar";

import Routes from "./config/Routes";

function App() {
	return (
		<BrowserRouter>
			<Route render={props => (
				<>
					<Sidebar {...props}>
						<Routes />
					</Sidebar>
				</>
			)}/>
		</BrowserRouter>
	);
}

export default App;
