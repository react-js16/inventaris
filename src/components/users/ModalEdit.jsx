import React, { useState } from 'react';

import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Button,
    Stack,
    FormControl,
    FormLabel,
    Input,
    useToast,
} from '@chakra-ui/react';

import inventarisApi from '../../api/inventarisApi'

const ModalEdit = ({isOpen, onClose, id, name, email, users, active}) => {

    const [editName, setEditName] = useState('')
    const [editEmail, setEditEmail] = useState('')

    const toast = useToast();

    const handleEdit = (e) => {
        e.preventDefault();
        // console.log(editName ? editName : name);
        const formData = new FormData();
        formData.append('name', editName ? editName : name);
        formData.append('email', editEmail ? editEmail: email);

        const update = inventarisApi.updateUser(id, formData)
        update.then((res) => {
            console.log(res.data);
            if(res.data.success == true) {
                toast({
                    title: 'User Updated.',
                    description: "We've updated the user for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: false,
                })
                onClose();
                users(active)
            }
        }).catch((error) => {
            console.log(error)
            if(error.response.status == 401) {
                localStorage.removeItem('token');
                window.location.reload();
            }
        });
    }

    return (
        <Modal isCentered isOpen={isOpen} onClose={onClose}>
            <ModalOverlay
                bg='blackAlpha.300'
                backdropFilter='blur(10px) hue-rotate(90deg)'
            />
            <ModalContent>
                <ModalHeader>Edit User</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                        <Stack spacing={4} as="form" onSubmit={(e) => {handleEdit(e)}}>
                            <FormControl id="name">
                                <FormLabel>Name</FormLabel>
                                <Input
                                    type="text"
                                    defaultValue={name}
                                    onChange={(e) => setEditName(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="email">
                                <FormLabel>Email Address</FormLabel>
                                <Input
                                    type="email"
                                    defaultValue={email}
                                    onChange={(e) => setEditEmail(e.target.value)}
                                />
                            </FormControl>
                            <Button
                                colorScheme={'blue'}
                                type='submit'
                            >
                                Update
                            </Button>
                        </Stack>
                </ModalBody>
            </ModalContent>
        </Modal>
    );
};

export default ModalEdit;