import React from "react"

import {
    AlertDialog,
    AlertDialogOverlay,
    AlertDialogContent,
    AlertDialogHeader,
    AlertDialogBody,
    AlertDialogFooter,
    Button,
    useToast
} from '@chakra-ui/react'

import inventarisApi from "../../api/inventarisApi"

const Alert = ({isOpen, onClose, id, users, active}) => {

    const cancelRef = React.useRef()
    const toast = useToast()

    const handleDelete = (e) => {
        e.preventDefault();

        // console.log(id)
        const deleteUser = inventarisApi.deleteUser(id);
        deleteUser.then((res) => {
            if(res.data.success == true) {
                toast({
                    title: 'User Deleted.',
                    description: "We've deleted the user for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: false,
                })
                onClose();
                users(active)
            }
        }).catch((error) => {
            console.log(error)
            if(error.response.status == 401) {
                localStorage.removeItem('token');
                window.location.reload();
            }
        });
    }

    return (
        <AlertDialog
            isOpen={isOpen}
            leastDestructiveRef={cancelRef}
            onClose={onClose}
        >
            <AlertDialogOverlay>
                <AlertDialogContent>
                    <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                        Delete User
                    </AlertDialogHeader>

                    <AlertDialogBody>
                        Are you sure? You can't undo this action afterwards.
                    </AlertDialogBody>

                    <AlertDialogFooter>
                        <Button ref={cancelRef} onClick={onClose}>
                            Cancel
                        </Button>
                        <Button colorScheme='red' onClick={(e) => handleDelete(e)} ml={3}>
                            Delete
                        </Button>
                    </AlertDialogFooter>
                </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
    )
}

export default Alert