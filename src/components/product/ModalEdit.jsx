import React, { useState } from 'react';

import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Button,
    Stack,
    FormControl,
    FormLabel,
    Input,
    useToast,
    Textarea
} from '@chakra-ui/react';
import inventarisApi from '../../api/inventarisApi';

const ModalEdit = ({isOpen, onClose, products, active, product, description, price, id}) => {

    const [productEdit, setProductEdit] = useState('')
    const [descriptionEdit, setDescriptionEdit] = useState('')
    const [priceEdit, setPriceEdit] = useState('')

    const toast = useToast()

    const handleUpdate = (e) => {
        e.preventDefault()

        console.log(productEdit ? productEdit : product);
        const formData = new FormData()
        formData.append('nama_produk', productEdit ? productEdit : product)
        formData.append('deskripsi_produk', descriptionEdit ? descriptionEdit : description)
        formData.append('harga', priceEdit ? priceEdit : price)

        const updateData = inventarisApi.updateProduct(id, formData)
        updateData.then((res) => {
            console.log(res.data)
            if(res.data.success == true) {
                toast({
                    title: 'Product Updated.',
                    description: "We've updated the product for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: false,
                })
                onClose();
                products(active)
            }
        }).catch((error) => {
            console.log(error)
            if(error.response.status == 401) {
                localStorage.removeItem('token');
                window.location.reload();
            }
        });
    }

    return (
        <Modal isCentered isOpen={isOpen} onClose={onClose}>
            <ModalOverlay
                bg='blackAlpha.300'
                backdropFilter='blur(10px) hue-rotate(90deg)'
            />
            <ModalContent>
                <ModalHeader>Edit Product</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                        <Stack spacing={4} as="form" onSubmit={(e) => handleUpdate(e)}>
                            <FormControl id="product">
                                <FormLabel>Product</FormLabel>
                                <Input
                                    type="text"
                                    defaultValue={product}
                                    onChange={(e) => setProductEdit(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="description">
                                <FormLabel>Description</FormLabel>
                                <Textarea
                                    defaultValue={description}
                                    onChange={(e) => setDescriptionEdit(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Price</FormLabel>
                                <Input
                                    type="number"
                                    defaultValue={price}
                                    onChange={(e) => setPriceEdit(e.target.value)}
                                />
                            </FormControl>
                            <Button
                                colorScheme={'blue'}
                                type='submit'
                            >
                                Update
                            </Button>
                        </Stack>
                </ModalBody>
            </ModalContent>
        </Modal>
    );
};

export default ModalEdit;