import React, { useState } from 'react';

import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Button,
    Stack,
    FormControl,
    FormLabel,
    Input,
    useToast,
    Textarea
} from '@chakra-ui/react';

import inventarisApi from '../../api/inventarisApi';

const ModalAdd = ({isOpen, onClose, products, active}) => {

    const [product, setProduct] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const toast = useToast()

    const handleAdd = (e) => {
        e.preventDefault()

        const formData = new FormData()
        formData.append('nama_produk', product)
        formData.append('deskripsi_produk', description)
        formData.append('harga', price)

        const add = inventarisApi.addProduct(formData);
        add.then((res) => {
            console.log(res.data)
            if(res.data.success == true) {
                toast({
                    title: 'Product Added.',
                    description: "We've added the product for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: false,
                })
                onClose();
                products(active)
            }
        }).catch((error) => {
            console.log(error)
            if(error.response.status == 401) {
                localStorage.removeItem('token');
                window.location.reload();
            }
        });
    }

    return (
        <Modal isCentered isOpen={isOpen} onClose={onClose}>
            <ModalOverlay
                bg='blackAlpha.300'
                backdropFilter='blur(10px) hue-rotate(90deg)'
            />
            <ModalContent>
                <ModalHeader>Add Product</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                        <Stack spacing={4} as="form" onSubmit={(e) => handleAdd(e)}>
                            <FormControl id="product">
                                <FormLabel>Product</FormLabel>
                                <Input
                                    type="text"
                                    onChange={(e) => setProduct(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="description">
                                <FormLabel>Description</FormLabel>
                                <Textarea
                                    onChange={(e) => setDescription(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Price</FormLabel>
                                <Input
                                    type="number"
                                    onChange={(e) => setPrice(e.target.value)}
                                />
                            </FormControl>
                            <Button
                                colorScheme={'blue'}
                                type='submit'
                            >
                                Add
                            </Button>
                        </Stack>
                </ModalBody>
            </ModalContent>
        </Modal>
    );
};

export default ModalAdd;