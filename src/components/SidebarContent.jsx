import {
    CloseButton,
    useColorModeValue,
    Box,
    Flex,
    Text,
    Menu,
    MenuButton,
    MenuList,
    MenuItem
} from '@chakra-ui/react';
import React from 'react';
import NavItem from './NavItem';
import {
    FiHome,
    FiSettings,
    FiArchive
} from 'react-icons/fi';
import { Link } from 'react-router-dom';

const SidebarContent = ({ user, onClose, ...rest }) => {
    return (
        <Box
            transition="3s ease"
            bg={useColorModeValue('white', 'gray.900')}
            borderRight="1px"
            borderRightColor={useColorModeValue('gray.200', 'gray.700')}
            w={{ base: 'full', md: 60 }}
            pos="fixed"
            h="full"
            {...rest}
        >
            <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
                <Link to='/'>
                    <Text fontSize="2xl" fontFamily="monospace" fontWeight="bold">
                        Logo
                    </Text>
                </Link>
                <CloseButton display={{ base: 'flex', md: 'none' }} onClick={onClose} />
            </Flex>
            <NavItem icon={FiHome} href={'/'}>
                Home
            </NavItem>
            <NavItem icon={FiArchive} href={'/archive'}>
                Archive
            </NavItem>
            {
                user.role == 1 ? 
                    <NavItem icon={FiSettings} href={'/settings'}>
                        Settings
                    </NavItem>
                    :
                    null
            }
        </Box>
    );
};

export default SidebarContent;