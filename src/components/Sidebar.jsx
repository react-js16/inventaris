import { Box, Drawer, DrawerContent, useColorModeValue, useDisclosure } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import SidebarContent from './SidebarContent';
import MobileNav from './MobileNav';
import { useLocation } from 'react-router-dom';
import Login from '../pages/Login';
import inventarisApi from '../api/inventarisApi';
import Register from '../pages/Register';

const Sidebar = ({children}) => {

    const { isOpen, onOpen, onClose} = useDisclosure();
    
    const [user, setUser] = useState([]);

    const bg = useColorModeValue('gray.100', 'gray.900');

    const route = useLocation();

    useEffect(() => {
        if(route.pathname !== '/login' && route.pathname !== '/register') {
            const currentUser = inventarisApi.getCurrentUser();
            currentUser.then((res) => {
                // console.log(res.data)
                setUser(res.data);
            }).catch((error) => {
                // console.log(error.response.status);
                if(error.response.status == 401) {
                    localStorage.removeItem('token');
                    window.location.replace('/login');
                }
            })
        }
    }, []);

    if(route.pathname == '/login') {
        return (
            <Login/>
        )
    } else if(route.pathname == '/register') {
        return (
            <Register/>
        )
    }

    return (
        <Box minH="100vh" bg={bg}>
            <SidebarContent
                user={user}
                onClose={() => onClose}
                display={{ base: 'none', md: 'block' }}
            />
            <Drawer
                autoFocus={false}
                isOpen={isOpen}
                placement="left"
                onClose={onClose}
                returnFocusOnClose={false}
                onOverlayClick={onClose}
                size="full"
            >
                <DrawerContent>
                    <SidebarContent user={user} onClose={onClose} />
                </DrawerContent>
            </Drawer>
            {/* mobilenav */}
            <MobileNav onOpen={onOpen} user={user}/>
            <Box ml={{ base: 0, md: 60 }} p="4">
                {React.Children.map(children, (child) => 
                    React.cloneElement(child, {user})
                )}
            </Box>
        </Box>
    );
};

export default Sidebar;