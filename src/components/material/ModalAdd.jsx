import React, { useState } from 'react';

import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Button,
    Stack,
    FormControl,
    FormLabel,
    Input,
    useToast,
    Textarea
} from '@chakra-ui/react';

import inventarisApi from '../../api/inventarisApi';

const ModalAdd = ({isOpen, onClose, getdetail, id}) => {

    const [material, setMaterial] = useState('')
    const [price, setPrice] = useState('')
    const [qty, setQty] = useState('')
    const [link, setLink] = useState('')

    const toast = useToast()

    const handleAdd = (e) => {
        e.preventDefault()

        const formData = new FormData()
        formData.append('nama_material', material)
        formData.append('harga', price)
        formData.append('jumlah', qty)
        formData.append('link', link)
        formData.append('product_id', id)

        const add = inventarisApi.addMaterial(formData)
        add.then((res) => {
            // console.log(res.data)
            console.log(res.data)
            if(res.data.success == true) {
                toast({
                    title: 'Product Added.',
                    description: "We've added the product for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: false,
                })
                onClose();
                getdetail()
            }
        })
    }

    return (
        <Modal isCentered isOpen={isOpen} onClose={onClose}>
            <ModalOverlay
                bg='blackAlpha.300'
                backdropFilter='blur(10px) hue-rotate(90deg)'
            />
            <ModalContent>
                <ModalHeader>Add Material</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                        <Stack spacing={4} as="form" onSubmit={(e) => handleAdd(e)}>
                            <FormControl id="product">
                                <FormLabel>Material</FormLabel>
                                <Input
                                    type="text"
                                    onChange={(e) => setMaterial(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Price</FormLabel>
                                <Input
                                    type="number"
                                    onChange={(e) => setPrice(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Qty</FormLabel>
                                <Input
                                    type="number"
                                    onChange={(e) => setQty(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Link</FormLabel>
                                <Input
                                    type="text"
                                    onChange={(e) => setLink(e.target.value)}
                                />
                            </FormControl>
                            <Button
                                colorScheme={'blue'}
                                type='submit'
                            >
                                Add
                            </Button>
                        </Stack>
                </ModalBody>
            </ModalContent>
        </Modal>
    );
};

export default ModalAdd;