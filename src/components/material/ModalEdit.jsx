import React, { useState } from 'react';

import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Button,
    Stack,
    FormControl,
    FormLabel,
    Input,
    useToast,
    Textarea
} from '@chakra-ui/react';
import inventarisApi from '../../api/inventarisApi';

const ModalEdit = ({isOpen, onClose, getdetail, id, Id, material, price, qty, link}) => {

    const [editMaterial, setEditMaterial] = useState()
    const [editPrice, setEditPrice] = useState()
    const [editQty, setEditQty] = useState()
    const [editLink, setEditLink] = useState()

    const toast = useToast()

    const handleUpdate = (e) => {
        e.preventDefault()

        const formData = new FormData()
        formData.append('nama_material', editMaterial ? editMaterial : material)
        formData.append('harga', editPrice ? editPrice : price)
        formData.append('jumlah', editQty ? editQty : qty)
        formData.append('link', editLink ? editLink : link)
        formData.append('product_id', id)

        const update = inventarisApi.updateMaterial(Id, formData)
        update.then((res) => {
            console.log(res)
            if(res.data.success == true) {
                toast({
                    title: 'Material Updated.',
                    description: "We've updated the material for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: false,
                })
                onClose();
                getdetail(id)
            }
        })
    }

    return (
        <Modal isCentered isOpen={isOpen} onClose={onClose}>
            <ModalOverlay
                bg='blackAlpha.300'
                backdropFilter='blur(10px) hue-rotate(90deg)'
            />
            <ModalContent>
                <ModalHeader>Edit Material</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                        <Stack spacing={4} as="form" onSubmit={(e) => handleUpdate(e)}>
                            <FormControl id="product">
                                <FormLabel>Material</FormLabel>
                                <Input
                                    defaultValue={material}
                                    type="text"
                                    onChange={(e) => setEditMaterial(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Price</FormLabel>
                                <Input
                                    defaultValue={price}
                                    type="number"
                                    onChange={(e) => setEditPrice(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Qty</FormLabel>
                                <Input
                                    defaultValue={qty}
                                    type="number"
                                    onChange={(e) => setEditQty(e.target.value)}
                                />
                            </FormControl>
                            <FormControl id="price">
                                <FormLabel>Link</FormLabel>
                                <Input
                                    defaultValue={link}
                                    type="text"
                                    onChange={(e) => setEditLink(e.target.value)}
                                />
                            </FormControl>
                            <Button
                                colorScheme={'blue'}
                                type='submit'
                            >
                                Update
                            </Button>
                        </Stack>
                </ModalBody>
            </ModalContent>
        </Modal>
    );
};

export default ModalEdit;