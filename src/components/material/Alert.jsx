import React from 'react';

import {
    AlertDialog,
    AlertDialogOverlay,
    AlertDialogContent,
    AlertDialogHeader,
    AlertDialogBody,
    AlertDialogFooter,
    Button,
    useToast
} from '@chakra-ui/react'
import inventarisApi from '../../api/inventarisApi';

const Alert = ({isOpen, onClose, Id, getdetail, id}) => {

    const cancelRef = React.useRef()
    const toast = useToast()

    const handleDelete = e => {
        e.preventDefault()
        
        const deleteMaterial = inventarisApi.deleteMaterial(Id)
        deleteMaterial.then((res) => {
            if(res.data.success == true) {
                toast({
                    title: 'Material Deleted.',
                    description: "We've deleted the material for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: false,
                })
                onClose();
                getdetail(id)
            }
        }).catch((error) => {
            console.log(error)
            if(error.response.status == 401) {
                localStorage.removeItem('token');
                window.location.reload();
            }
        });
    }

    return (
        <AlertDialog
            isOpen={isOpen}
            leastDestructiveRef={cancelRef}
            onClose={onClose}
        >
            <AlertDialogOverlay>
                <AlertDialogContent>
                    <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                        Delete Material
                    </AlertDialogHeader>

                    <AlertDialogBody>
                        Are you sure? You can't undo this action afterwards.
                    </AlertDialogBody>

                    <AlertDialogFooter>
                        <Button ref={cancelRef} onClick={onClose}>
                            Cancel
                        </Button>
                        <Button colorScheme='red' ml={3} onClick={e => handleDelete(e)}>
                            Delete
                        </Button>
                    </AlertDialogFooter>
                </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
    );
};

export default Alert;